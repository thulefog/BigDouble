# BigDouble

A data capture and storage backend.

# Context:

The context for the small system can be illustrated as follows:

## Front End:
There is a mobile application that captures data - for example a JSON file that serialed representation of an object in the Type System.

The JSON instance is sent as a stream to the REST Endnpoint

##  Back End:
There are two main design surfaces in this code back end - a REST endpoint listener and a small data visualization UI in C++ based on Qt intended to provide the abilty to do a quick verify of data received.

An example of this _Data Visualization_ is shown below.

![ExampleChart](./Presentation/linechart/ExampleChart.png)

The code behind this repository originally  had submodules and was seeded against the __datasift/served__ C++ library with a science project against Tesseract. 

In that case - Tesseract was used to screen scrape lab results from a screen capture from Kaiser's mobile for routtine blood work - again, as a science project.

### REST Endpoint

The REST service endpoint is curently based primarily on cpprestsdk, was an experiment compare against previous spikes against the __datasift/served__ C++ library .

###  [Microsoft/cpprestsdk](https://github.com/Microsoft/cpprestsdk.git)

https://github.com/Microsoft/cpprestsdk/wiki/How-to-build-for-Mac-OS-X

> brew install boost cmake openssl libiconv

### [json-cpp](https://github.com/open-source-parsers/jsoncpp.git)

This JSON C++ serialization library may in the end be unnessary but added to potentially simplify JSON I/O.

### Notes about cpprest

The cpprest samples depicting a server are not that great. I ended up with a pared down approach like *restweb*. The other minimal listener munged the incoming JSON stream big time.

The key kung fu is this lambda syntax:

~~~
void Listener::PUT_RequestHandler(http_request request)
{
auto method = __func__;
TRACE( method );

request.extract_json().then([](pplx::task<json::value> task) {
try
{
auto const & value = task.get();
cout << value.serialize() << endl;
...
~~~

* https://github.com/Meenapintu/Restweb

* https://medium.com/audelabs/modern-c-micro-service-implementation-rest-api-b499ffeaf898

* https://github.com/ivanmejiarocha/micro-service

* https://mariusbancila.ro/blog/2013/08/19/full-fledged-client-server-example-with-cpprest-sdk-110/


## Scripts

__Build__

To build the cpprestsdk...

~~~
mkdir build.debug
cd build.debug
cmake ../Release -DCMAKE_BUILD_TYPE=Debug -DWERROR=OFF 
make -j 4 
~~~

__Verify__

The purpose of the script *verify_listener.sh* is to verify that the TCP port has the listener bound and in LISTEN mode. Reference excerpt below.

>  sudo lsof -i -n -P | grep TCP | grep endpoint

...with output below:
~~~
endpoint  64251         <user>    8u  IPv4 0xe74bf316ae50006b      0t0    TCP 127.0.0.1:63061 (LISTEN)
~~~

