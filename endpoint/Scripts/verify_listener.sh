#!/bin/sh

#  verify_listener.sh
#  endpoint
#
#  Created by John Matthew Weston on 7/1/18.
#  Copyright © 2018 John Matthew Weston. All rights reserved.

sudo lsof -i -n -P | grep TCP | grep endpoint
