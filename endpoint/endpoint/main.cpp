
#include "Adapters/cpprest/listener.hpp"

int main()
{

    Listener *listener;
    
	try
	{
        utility::string_t address = "http://localhost:60000/endpoint";
        listener = new Listener( address );
        listener->open().wait();
    }
    catch (exception const & e)
	{
		wcout << e.what() << endl;
	}

    std::cout << "Press ENTER to exit." << std::endl;
    
    std::string line;
    std::getline(std::cin, line);
    
    listener->close().wait();
    
	return 0;
}
