//
//  listener.cpp
//  endpoint
//
//  Created by John Matthew Weston on 7/1/18.
//  Copyright © 2018 John Matthew Weston. All rights reserved.
//

#include "listener.hpp"

Listener::Listener()
{
    
}

Listener::Listener( utility::string_t url):_listener(url)
{
    _listener.support(methods::GET, std::bind(&Listener::GET_RequestHandler, this, std::placeholders::_1));
    _listener.support(methods::PUT, std::bind(&Listener::PUT_RequestHandler, this, std::placeholders::_1));
    _listener.support(methods::POST, std::bind(&Listener::POST_RequestHandler, this, std::placeholders::_1));
    _listener.support(methods::DEL, std::bind(&Listener::DEL_RequestHandler, this, std::placeholders::_1));
}

Listener::~Listener()
{
    
}

void Listener::GET_RequestHandler(http_request request)
{
    auto method = __func__;
    TRACE( method );
    
    ucout <<  request.to_string() << endl;
    
    auto paths = http::uri::split_path(http::uri::decode(request.relative_uri().path()));
    
    request.relative_uri().path();

    string response = U("GET_REQUEST_RECEIVED");
    request.reply(status_codes::OK, response);
}

void Listener::POST_RequestHandler(http_request request)
{
    auto method = __func__;
    TRACE( method );

    cout <<  request.to_string() << endl;
    request.reply(status_codes::OK,request.to_string());
}

void Listener::PUT_RequestHandler(http_request request)
{
    auto method = __func__;
    TRACE( method );
    
    request.extract_json().then([](pplx::task<json::value> task) {
        try
        {
            auto const & value = task.get();
            cout << value.serialize() << endl;
            
            ofstream outputFileStream;
            outputFileStream.open ("PUT.JSON");
            outputFileStream << value.serialize();
            outputFileStream.close();
        }
        catch (http_exception const & e)
        {
            wcout << e.what() << endl;
        }
    });
    
    cout << "____" << endl;
    ucout <<  request.to_string() << endl;
    cout << "____" << endl;
    string response = U("PUT_REQUEST_RECEIVED");
    request.reply(status_codes::OK,response);
    return;
}

void Listener::DEL_RequestHandler(http_request request)
{
    auto method = __func__;
    TRACE( method );
    
}
