//
//  listener.hpp
//  endpoint
//
//  Created by John Matthew Weston on 7/1/18.
//  Copyright © 2018 John Matthew Weston. All rights reserved.
//

#ifndef listener_hpp
#define listener_hpp

#include <stdio.h>

#include <cpprest/http_listener.h>
#include <cpprest/json.h>

using namespace web;
using namespace web::http;
using namespace web::http::experimental::listener;

#include <iostream>
#include <map>
#include <set>
#include <string>

#include <cstring>
using namespace std;

#define TRACE(msg)            wcout << msg << endl

class Listener
{
    public:
        Listener();
        Listener(utility::string_t url);
        virtual ~Listener();
    
        pplx::task<void>open(){return _listener.open();}
        pplx::task<void>close(){return _listener.close();}
    
    protected:
    
    private:
        void GET_RequestHandler(http_request request);
    
        void POST_RequestHandler(http_request request);
    
        void PUT_RequestHandler(http_request request);
    
        void DEL_RequestHandler(http_request request);

        void ErrorHandler(pplx::task<void>& t);
    
        http_listener _listener;
};


#endif /* listener_hpp */
